package websk

import (
	"sync"

	"github.com/gorilla/websocket"
)

// Connnection is fuck
type Connection struct {
	conn  *websocket.Conn
	ID    string
	send  chan []byte
	mutex *sync.Mutex
	box   *Box
}

// Box a hole for connections
type Box struct {
	box       map[*Connection]bool
	broadcast chan []byte
	action    chan *Action
}

type Action struct {
	Name string
	Con  *Connection
}

// InitWsPool create pool
func (b *Box) InitBox() {
	b.box = make(map[*Connection]bool)
	b.broadcast = make(chan []byte)
	b.action = make(chan *Action)
}

func (b *Box) interactive(action *Action) {
	switch action.Name {
	case "register":
		b.box[action.Con] = true
	case "unregister":
		if _, ok := b.box[action.Con]; ok {
			action.Con.mutex.Unlock()
			close(action.Con.send)
			delete(b.box, action.Con)
		}
	case "disconnect":
		b.box[action.Con] = false
	}
}

// Start Box
func (b *Box) Start() {
	for {
		select {
		// receive from action system action
		case action := <-b.action:
			b.interactive(action)
			// broadcast action
		case msg := <-b.broadcast:
			for con := range b.box {
				select {
				case con.send <- msg:
				default:
					close(con.send)
					delete(b.box, con)
				}
			}
		}
	}
}
