package websk

import (
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/my0sot1s/helper"
)

const (
	maxMessageSize = 1024
	waitDuration   = 3 * 60 * time.Second
	writeWait      = 10 * time.Second
	pingPeriod     = (waitDuration * 9) / 10
	limitResent    = 4
)

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  maxMessageSize * 2,
		WriteBufferSize: maxMessageSize * 2,
	}
)

// GetID just get ID
func (c Connection) GetID() string {
	return c.ID
}

// InitConnection create new Conn
func (c *Connection) InitConnection(box *Box, w http.ResponseWriter, r *http.Request) {
	// go c.listenEvent()
	if conn, err := upgrader.Upgrade(w, r, nil); err == nil {
		c.conn = conn
	}
	c.mutex = &sync.Mutex{}
	c.send = make(chan []byte, maxMessageSize)
	c.box = box
}

func (c *Connection) beforeRead() {
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(waitDuration))
	c.conn.SetPongHandler(func(string) error {
		c.conn.SetReadDeadline(time.Now().Add(waitDuration))
		return nil
	})
}

func (c *Connection) verifyConnector() {
	ticker := time.NewTicker(pingPeriod)
	go func() {
		c.conn.SetWriteDeadline(time.Now().Add(writeWait))
		if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
			return
		}
	}()
}

// Read is a action Read message from any room
func (c *Connection) Read() {
	defer func() {
		c.pool.action <- &Action{Name: "unregister", Con: c}
		c.conn.Close()
	}()
	c.beforeRead()
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err,
				websocket.CloseGoingAway,
				websocket.CloseAbnormalClosure) {
				helper.ErrLog(err)
			}
			break
		}
		c.pool.broadcast <- message
	}
}

// WriteMessageData is broad cast data to a room
func (c *Connection) Write() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			// close conn when error ws
			if !ok {
				c.conn.WriteMessage(websocket.CloseMessage, nil)
				return
			}
			w, err := c.conn.NextWriter(websocket.BinaryMessage)
			if err != nil {
				return
			}
			w.Write(message)
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			// ping Client
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}
