package websk

import "encoding/json"

// Message is struct message
type Message struct {
	ID      string `json:"id"`
	Type    string `json:"type"`
	Text    string `json:"text"`
	Created int    `json:"created"`
	// By is author
	By string `json:"by"`
	// To is room
	To string `json:"to"`
}

func (m Message) toByte() []byte {
	b, _ := json.Marshal(m)
	return b
}
