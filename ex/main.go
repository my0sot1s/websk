package main

import (
	"flag"
	"log"
	"net/http"

	"gitlab.com/my0sot1s/websk"
)

var addr = flag.String("addr", ":8080", "http service address")

func main() {
	box := &websk.Box{}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			http.Error(w, "Not found", http.StatusNotFound)
			return
		}
		if r.Method != "GET" {
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}
		http.ServeFile(w, r, "index.html")
	})
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		conn := &websk.Connection{}
		conn.InitConnection(pool, w, r)
		go conn.Read()
		go conn.Write()
	})
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
